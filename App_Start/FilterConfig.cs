﻿using System.Web;
using System.Web.Mvc;

namespace Phoenix_Petstore
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
