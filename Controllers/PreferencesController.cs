﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class PreferencesController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/preference
        public IEnumerable<Preference> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Preferences.ToList(); 

        }

        [HttpGet]
        [Route("api/preference/{name}")]
        //GET: api/preference/name
        public Preference Get(string name) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Preferences.Where(c => c.KEYID.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/preference
        public void Post(Preference supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.Preferences.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/preference/{name}")]
        public void Delete(string name) {
            db.Configuration.ProxyCreationEnabled = false;

            Preference preference = 
                (Preference)db.Preferences.Where(c => c.KEYID.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            db.Preferences.Remove(preference);
            db.SaveChanges();
        }

    }
}