﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers {
    public class CustomerAccountController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/customeraccount")]
        //GET: api/customeraccount
        public IEnumerable<CustomerAccount> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.CustomerAccounts.ToList();

        }

        [HttpGet]
        [Route("api/customeraccount/{id}")]
        //GET: api/customeraccount/id
        public CustomerAccount Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.CustomerAccounts.Where(c => c.ACCOUNTID == id).FirstOrDefault();
        }

        [HttpPost]
        [Route("api/customeraccount")]
        //POST: api/customeraccount
        public void Post(CustomerAccount orderItem) {
            db.Configuration.ProxyCreationEnabled = false;

            db.CustomerAccounts.Add(orderItem);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/customeraccount/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            CustomerAccount customeraccounts = (CustomerAccount)db.CustomerAccounts.Where(c => c.ACCOUNTID == id).FirstOrDefault();

            db.CustomerAccounts.Remove(customeraccounts);
            db.SaveChanges();
        }

    }
}
