﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class MerchandiseOrderController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/MerchandiseOrder")]
        //GET: api/MerchandiseOrder
        public IEnumerable<MerchandiseOrder> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.MerchandiseOrders.ToList();

        }

        [HttpGet]
        [Route("api/MerchandiseOrder/{id}")]
        //GET: api/MerchandiseOrder/id
        public MerchandiseOrder Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.MerchandiseOrders.Where(c => c.PONUMBER == id).FirstOrDefault();
        }

        [HttpPost]
        [Route("api/MerchandiseOrder")]
        //POST: api/MerchandiseOrder
        public void Post(MerchandiseOrder merchandiseOrder) {
            db.Configuration.ProxyCreationEnabled = false;

            db.MerchandiseOrders.Add(merchandiseOrder);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/MerchandiseOrder/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            MerchandiseOrder merchandiseOrder = (MerchandiseOrder)db.MerchandiseOrders.Where(c => c.PONUMBER == id).FirstOrDefault();

            db.MerchandiseOrders.Remove(merchandiseOrder);
            db.SaveChanges();
        }
    }
}
