﻿using Phoenix_Petstore.Models;
using Phoenix_Petstore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class AnimalController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();


        [HttpGet]
        [Route("api/animal")]
        //GET: api/animal
        public IHttpActionResult Get()
        {
            db.Configuration.ProxyCreationEnabled = false;


            var animals = db.Animals.Select(a => new AnimalVM()
            { 
                NAME = a.NAME,
                BREED = a.BREED,
                DATEBORN = a.DATEBORN,
                GENDER = a.GENDER,
                CATEGORY = a.CATEGORY,
                COLOR = a.COLOR,
                LISTPRICE = a.LISTPRICE,
                IMAGEWIDTH = a.IMAGEWIDTH,
                IMAGEFILE = a.IMAGEFILE,
                REGISTERED = a.REGISTERED,
                IMAGEHEIGHT = a.IMAGEHEIGHT,
                PHOTO = a.PHOTO
            }).ToList<AnimalVM>();
      
            return Ok(animals);
         
        }


        [HttpGet]
        [Route("api/animal/{id}")]
        //GET: api/animal/2  
        public Animal Get(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Animals.Where(c => c.ANIMALID == id).FirstOrDefault();
        }


        [HttpPost]
        [Route("api/Animal")]
        //POST: api/Animal
        public void Post(Animal animal)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Animals.Add(animal);
            db.SaveChanges();
        }



        [HttpDelete]
        [Route("api/Animal/{id}")]
        public void Delete(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Animal animal= db.Animals.Where(c => c.ANIMALID == id).FirstOrDefault();

            db.Animals.Remove(animal);
            db.SaveChanges();
        }
    }








}
