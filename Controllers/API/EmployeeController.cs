﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers {
    public class EmployeeController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/employee")]
        //GET: api/employee
        public IEnumerable<Employee> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Employees.ToList();

        }

        [HttpGet]
        [Route("api/employee/{id}")]
        //GET: api/employee/id
        public Employee Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Employees.Where(c => c.EMPLOYEEID == id).FirstOrDefault();
        }

        [HttpPost]
        [Route("api/employee")]
        //POST: api/employee
        public void Post(Employee emp) {
            db.Configuration.ProxyCreationEnabled = false;

            db.Employees.Add(emp);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/employee/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            Employee emp = (Employee)db.Employees.Where(c => c.EMPLOYEEID == id).FirstOrDefault();

            db.Employees.Remove(emp);
            db.SaveChanges();
        }

    }
}
