﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class BreedController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/breed
        public IEnumerable<Breed> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Breeds.ToList();

        }

        [HttpGet]
        [Route("api/breed/{name}")]
        //GET: api/breed/Canary
        public Breed Get(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Breeds.Where(c => c.BREED1.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/breed
        public void Post(Breed breed)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Breeds.Add(breed);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/breed/{name}")]
        public void Delete(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            Breed breed = (Breed)db.Breeds.Where(c => c.BREED1.Equals(name)).FirstOrDefault();

            db.Breeds.Remove(breed);
            db.SaveChanges();
        }


    }
}
