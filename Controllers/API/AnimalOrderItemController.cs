﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class AnimalOrderItemController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/AnimalOrderItem")]
        //GET: api/AnimalOrderItem
        public IEnumerable<AnimalOrderItem> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AnimalOrderItems.ToList();

        }

        [HttpGet]
        [Route("api/AnimalOrderItem/{id}")]
        //GET: api/AnimalOrderItem/id
        public AnimalOrderItem Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AnimalOrderItems.Where(c => c.ANIMALID == id).FirstOrDefault();
        }

        [HttpPost]
        [Route("api/AnimalOrderItem")]
        //POST: api/AnimalOrderItem
        public void Post(AnimalOrderItem animalOrderItem) {
            db.Configuration.ProxyCreationEnabled = false;

            db.AnimalOrderItems.Add(animalOrderItem);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/AnimalOrderItem/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            AnimalOrderItem animalOrderItem = (AnimalOrderItem)db.AnimalOrderItems.Where(c => c.ANIMALID == id).FirstOrDefault();

            db.AnimalOrderItems.Remove(animalOrderItem);
            db.SaveChanges();
        }



    }
}
