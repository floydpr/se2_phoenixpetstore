﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class OrderItemController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/OrderItem")]
        //GET: api/breed
        public IEnumerable<OrderItem> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.OrderItems.ToList();

        }

        [HttpGet]
        [Route("api/OrderItem/{id}")]
        //GET: api/OrderItem/id
        public OrderItem Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.OrderItems.Where(c => c.ITEMID == id).FirstOrDefault();
        }

        [HttpPost]
        [Route("api/OrderItem")]
        //POST: api/OrderItem
        public void Post(OrderItem orderItem) {
            db.Configuration.ProxyCreationEnabled = false;

            db.OrderItems.Add(orderItem);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/OrderItem/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            OrderItem orderItem = (OrderItem)db.OrderItems.Where(c => c.ITEMID == id).FirstOrDefault();

            db.OrderItems.Remove(orderItem);
            db.SaveChanges();
        }

    }
}
