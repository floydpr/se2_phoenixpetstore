﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Phoenix_Petstore.Controllers
{
    public class MerchandiseController : ApiController
    {
        
        private PetstoreDbContext db = new PetstoreDbContext();


        [HttpGet]
        [Route("api/merchandise")]
        //GET: api/merchandise
        public IEnumerable<Merchandise> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Merchandises.ToList();
            
        }



        [HttpGet]
        [Route("api/merchandise/{id}")]
        //GET: api/merchandise/5  
        public Merchandise Get(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Merchandises.Where(c => c.ITEMID == id).FirstOrDefault();
        }


        [HttpPost]
        [Route("api/merchandise")]
        //POST: api/merchandise
        public void Post(Merchandise merchandise)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Merchandises.Add(merchandise);
            db.SaveChanges();
        }



        [HttpDelete]
        [Route("api/merchandise/{id}")]
        public void Delete(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Merchandise merchandise = db.Merchandises.Where(c => c.ITEMID == id).FirstOrDefault();

            db.Merchandises.Remove(merchandise);
            db.SaveChanges();
        }
    }
}