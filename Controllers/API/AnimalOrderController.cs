﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class AnimalOrderController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();


        [HttpGet]
         //GET: api/AnimalOrder
        public IEnumerable<AnimalOrder> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AnimalOrders.ToList();
        }

        [HttpGet]
        [Route("api/animalorder/{id}")]
        //GET: api/animalorder/5
        public AnimalOrder Get(Byte id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AnimalOrders.Where(c => c.ORDERID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/animalorder
        public void Post(AnimalOrder animalOrder)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.AnimalOrders.Add(animalOrder);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/animalorder/{id}")]
        public void Delete(Byte id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            AnimalOrder animalOrder = (AnimalOrder)db.AnimalOrders.Where(c => c.ORDERID == id).FirstOrDefault();

            db.AnimalOrders.Remove(animalOrder);
            db.SaveChanges();
        }
    }
}
