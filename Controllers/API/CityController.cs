﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class CityController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/city
        public IEnumerable<City> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Cities.ToList();

        }

        [HttpGet]
        [Route("api/city/{id}")]
        //GET: api/city/5855
        public City Get(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Cities.Where(c => c.CITYID == id).FirstOrDefault();
        }
        
        [HttpPost]
        //POST: api/city
        public void Post(City city)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Cities.Add(city);
            db.SaveChanges();
        }
        
        [HttpDelete]
        [Route("api/city/{id}")]
        public void Delete(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            City city = (City)db.Cities.Where(c => c.CITYID == id).FirstOrDefault();

            db.Cities.Remove(city);
            db.SaveChanges();
        }
    }
}
