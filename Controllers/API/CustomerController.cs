﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class CustomerController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        [Route("api/customer")]
        //GET: api/customer
        public IEnumerable<Customer> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Customers.ToList();

        }

        [HttpGet]
        [Route("api/customer/{id}")]
        //GET: api/customer/1
        public Customer Get(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Customers.Where(c => c.CUSTOMERID == id).FirstOrDefault();
        }
        
        [HttpPost]
        [Route("api/customer")]
        //POST: api/customer
        public void Post(Customer customer)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Customers.Add(customer);
            db.SaveChanges();
        }
       
        [HttpDelete]
        [Route("api/customer/{id}")]
        public void Delete(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            Customer customer = (Customer)db.Customers.Where(c => c.CUSTOMERID == id).FirstOrDefault();

            db.Customers.Remove(customer);
            db.SaveChanges();
        }
    }
}
