﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers
{
    public class CategoryController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/category
        public IEnumerable<Category> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Categories.ToList();

        }

        [HttpGet]
        [Route("api/category/{name}")]
        //GET: api/category/Cat
        public Category Get(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Categories.Where(c => c.CATEGORY1.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/category
        public void Post(Category category)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Categories.Add(category);
            db.SaveChanges();   
        }


        [HttpDelete]
        [Route("api/category/{name}")]
        public void Delete(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            Category category = (Category)db.Categories.Where(c => c.CATEGORY1.Equals(name)).FirstOrDefault();

            db.Categories.Remove(category);
            db.SaveChanges();
        }

    }
}
