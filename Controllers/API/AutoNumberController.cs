﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Phoenix_Petstore.Controllers
{
    public class AutoNumberController : ApiController
    {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/autonumber
        public IEnumerable<AutoNumber> Get()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AutoNumbers.ToList();

        }

        [HttpGet]
        [Route("api/autonumber/{name}")]
        //GET: api/autonumber/Animal
        public AutoNumber Get(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.AutoNumbers.Where(c => c.TABLENAME.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/autonumber
        public void Post(AutoNumber autoNumber)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.AutoNumbers.Add(autoNumber);
            db.SaveChanges();
        }

        [HttpDelete]
        [Route("api/autonumber/{name}")]
        public void Delete(string name)
        {
            db.Configuration.ProxyCreationEnabled = false;

            AutoNumber autoNumber = (AutoNumber)db.AutoNumbers.Where(c => c.TABLENAME.Equals(name)).FirstOrDefault();

            db.AutoNumbers.Remove(autoNumber);
            db.SaveChanges();
        }

    }
}
