﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class SaleAnimalController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/saleanimal
        public IEnumerable<SaleAnimal> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.SaleAnimals.ToList(); 

        }

        [HttpGet]
        [Route("api/saleanimal/{id}")]
        //GET: api/saleanimal/id
        public SaleAnimal Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.SaleAnimals.Where(c => c.ANIMALID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/saleanimal
        public void Post(SaleAnimal supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.SaleAnimals.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/saleanimal/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            SaleAnimal saleanimal = (SaleAnimal)db.SaleAnimals.Where(c => c.ANIMALID == id).FirstOrDefault();

            db.SaleAnimals.Remove(saleanimal);
            db.SaveChanges();
        }

    }
}