﻿using Newtonsoft.Json;
using Phoenix_Petstore.Models;
using Phoenix_Petstore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix_Petstore.Controllers
{
    public class HomeController : Controller
    {
        private static TimeZoneInfo Eastern_Standard_Time = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");


        [Obsolete]
        public ActionResult Index()
        {

            string host = Dns.GetHostName();
            string ipAddress = Dns.GetHostByName(host).AddressList[0].ToString();

            DateTime timestamp = DateTime.Now;

            var os = System.Runtime.InteropServices.RuntimeInformation.OSDescription;

            ViewBag.IP = ipAddress;
            ViewBag.TimeStamp = timestamp;
            ViewBag.OS = os;

            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

       
        // Code source: https://www.compilemode.com/2020/12/get-data-in-aspnet-mvc-using-web-api.html

        [Obsolete]
        public async Task<ActionResult> Animal()
        {
            string port = Request.Url.Port.ToString();
            string Baseurl = "https://localhost:" + port + "/";

            ViewBag.Port = port;

            List<Animal> AnimalInfo = new List<Animal>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/Animal");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var AnimalResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    AnimalInfo = JsonConvert.DeserializeObject<List<Animal>>(AnimalResponse);

                }
                //returning the employee list to view  


                //POST
                string host = Dns.GetHostName();
                string ipAddress = Dns.GetHostByName(host).AddressList[0].ToString();

            

                DateTime timestamp = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Eastern_Standard_Time);
                var os = System.Runtime.InteropServices.RuntimeInformation.OSDescription.ToString();
                     

                var visit = new Visit(ipAddress, timestamp, os);

                var json = JsonConvert.SerializeObject(visit);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var url = "https://monitoringprojfix1.azurewebsites.net/api/visits";
                using (var clientP = new HttpClient())
                {
                    var response = await clientP.PostAsync(url, data);

                    var result = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(result);

                };

                return View(AnimalInfo);
            }

        }

        public async Task<ActionResult> Merchandise() {
            string port = Request.Url.Port.ToString();
            string Baseurl = "https://localhost:" + port + "/";

            ViewBag.Port = port;

            List<Merchandise> MerchandiseInfo = new List<Merchandise>();

            using (var client = new HttpClient()) {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/Merchandise");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode) {
                    //Storing the response details recieved from web api   
                    var MerchandiseResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    MerchandiseInfo = JsonConvert.DeserializeObject<List<Merchandise>>(MerchandiseResponse);

                }
                //returning the employee list to view  
                return View(MerchandiseInfo);
            }

        }

    }
         
}