﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class SupplierController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/supplier
        public IEnumerable<Supplier> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Suppliers.ToList(); 

        }

        [HttpGet]
        [Route("api/supplier/{id}")]
        //GET: api/supplier/id
        public Supplier Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Suppliers.Where(c => c.SUPPLIERID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/supplier
        public void Post(Supplier supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.Suppliers.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/supplier/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            Supplier supplier = (Supplier)db.Suppliers.Where(c => c.SUPPLIERID == id).FirstOrDefault();

            db.Suppliers.Remove(supplier);
            db.SaveChanges();
        }

    }
}