﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class SaleItemController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/saleitem
        public IEnumerable<SaleItem> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.SaleItems.ToList(); 

        }

        [HttpGet]
        [Route("api/saleitem/{id}")]
        //GET: api/supplier/id
        public SaleItem Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.SaleItems.Where(c => c.SALEID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/saleitem
        public void Post(SaleItem supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.SaleItems.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/saleitem/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            SaleItem saleitem = (SaleItem)db.SaleItems.Where(c => c.SALEID == id).FirstOrDefault();

            db.SaleItems.Remove(saleitem);
            db.SaveChanges();
        }

    }
}