﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class SaleController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/sale
        public IEnumerable<Sale> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Sales.ToList(); 

        }

        [HttpGet]
        [Route("api/sale/{id}")]
        //GET: api/sale/id
        public Sale Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Sales.Where(c => c.SALEID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/sale
        public void Post(Sale supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.Sales.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/sale/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            Sale sale = (Sale)db.Sales.Where(c => c.SALEID == id).FirstOrDefault();

            db.Sales.Remove(sale);
            db.SaveChanges();
        }

    }
}