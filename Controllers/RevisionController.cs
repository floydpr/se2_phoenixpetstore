﻿using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Phoenix_Petstore.Controllers {
    public class RevisionController : ApiController {
        private PetstoreDbContext db = new PetstoreDbContext();

        [HttpGet]
        //GET: api/revision
        public IEnumerable<Revision> Get() {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Revisions.ToList(); 

        }

        [HttpGet]
        [Route("api/revision/{id}")]
        //GET: api/revision/id
        public Revision Get(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Revisions.Where(c => c.REVISIONID == id).FirstOrDefault();
        }

        [HttpPost]
        //POST: api/revision
        public void Post(Revision supplier) {
            db.Configuration.ProxyCreationEnabled = false;

            db.Revisions.Add(supplier);
            db.SaveChanges();
        }


        [HttpDelete]
        [Route("api/revision/{id}")]
        public void Delete(int id) {
            db.Configuration.ProxyCreationEnabled = false;

            Revision revision = (Revision)db.Revisions.Where(c => c.REVISIONID == id).FirstOrDefault();

            db.Revisions.Remove(revision);
            db.SaveChanges();
        }

    }
}