using NUnit.Framework;
using Phoenix_Petstore;
using Phoenix_Petstore.Controllers;
using Phoenix_Petstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace TestProject1
{
    public class Tests
    {
       
        [Test]
        public void TestBreedGet()
        {
            var bc = new BreedController();
            string i = "Canary";
            Assert.NotNull(bc.Get(i));
        }

        [Test]
        [Ignore("Ignore a test")]
        public void TestBreedPost()
        {
            PetstoreDbContext db = new PetstoreDbContext();
            var bc = new BreedController();
            Breed breed = new Breed();
            breed.CATEGORY = "Canary";
            bc.Post(breed);
            Assert.NotNull(bc.Get("Canary"));
        }
        // TODO: test the post


        [Test]
        [Ignore("Ignore a test")]
        public void TestBreedDelete()           // fails 
        {
           
            var bc = new BreedController();
            string i = "Canary";
            bc.Delete(i);
            Assert.IsNull(bc.Get(i));
        }

        /// <summary>
        /// TestAnimalOrderController
        /// </summary>

        [Test]
        public void TestAnimalOrderControllerGet()
        {

            var ao = new AnimalOrderController();
            Byte i = 1;
            ao.Get(i);
            Assert.IsNotNull(ao.Get(i));
        }

        [Test]
        public void TestAnimalOrderControllerPost()
        {

            var ao = new AnimalOrderController();
            AnimalOrder animal = new AnimalOrder();
            animal.ORDERID = 97;
            ao.Post(animal);
            Assert.IsNotNull(ao.Get(97));
            //ao.Delete(97);
        }

        [Test]
        public void TestAnimalOrderControllerDelete()
        {

            var ao = new AnimalOrderController();
            AnimalOrder animal = new AnimalOrder();
            animal.ORDERID = 97;
            ao.Delete(97);
            Assert.IsNull(ao.Get(97));
        }

        /// <summary>
        /// MerchandiseController
        /// </summary>
        [Test]
        public void TestMerchandiseControllerGet()
        {

            var mc = new MerchandiseController();
            int id = 1;
            Assert.IsNotNull(mc.Get(id));
        }

        [Test]
        public void TestMerchandiseControllerPost()
        {
            var mc = new MerchandiseController();
            Merchandise merchandise = new Merchandise();
            merchandise.ITEMID = 102;
            mc.Post(merchandise);
            Assert.IsNotNull(mc.Get(102));
            //mc.Delete(101);
        }

        public void TestMerchandiseControllerDelete()
        {
            var mc = new MerchandiseController();
            Merchandise merchandise = new Merchandise();
            int id = 102;
            mc.Delete(id);
            Assert.IsNull(mc.Get(102));
            
        }

        /// <summary>
        /// AnimalController
        /// </summary>
        [Test]
        public void TestAnimalControllerGet()
        {

            var ac = new AnimalController();
            int id = 6;
            Assert.IsNotNull(ac.Get(id));
        }

        [Test]
        [Ignore("Ignore a test")]
        public void TestAnimalControllerPost()
        {
            var ac = new AnimalController();
            Animal animal= new Animal();
            animal.ANIMALID = 1;
            ac.Post(animal);
            Assert.IsNotNull(ac.Get(1));
            //ac.Delete(1);
        }

        [Test]
        [Ignore("Ignore a test")]
        public void TestAnimalControllerDelete()
        {
            var ac = new AnimalController();
            Animal animal = new Animal();
            int id = 7;
            ac.Delete(id);
            Assert.IsNull(ac.Get(7));

        }


    }
}