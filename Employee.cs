//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Petstore
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            this.AnimalOrders = new HashSet<AnimalOrder>();
            this.MerchandiseOrders = new HashSet<MerchandiseOrder>();
            this.Sales = new HashSet<Sale>();
        }
    
        public byte EMPLOYEEID { get; set; }
        public string LASTNAME { get; set; }
        public string FIRSTNAME { get; set; }
        public string PHONE { get; set; }
        public string ADDRESS { get; set; }
        public Nullable<int> ZIPCODE { get; set; }
        public Nullable<short> CITYID { get; set; }
        public string TAXPAYERID { get; set; }
        public Nullable<System.DateTime> DATEHIRED { get; set; }
        public string DATERELEASED { get; set; }
        public Nullable<byte> MANAGERID { get; set; }
        public Nullable<byte> EMPLOYEELEVEL { get; set; }
        public string TITLE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnimalOrder> AnimalOrders { get; set; }
        public virtual City City { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchandiseOrder> MerchandiseOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sale> Sales { get; set; }
    }
}
