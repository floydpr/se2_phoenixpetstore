//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Petstore
{
    using System;
    using System.Collections.Generic;
    
    public partial class City
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public City()
        {
            this.Customers = new HashSet<Customer>();
            this.Employees = new HashSet<Employee>();
            this.Suppliers = new HashSet<Supplier>();
        }
    
        public short CITYID { get; set; }
        public Nullable<int> ZIPCODE { get; set; }
        public string CITY1 { get; set; }
        public string STATE { get; set; }
        public Nullable<short> AREACODE { get; set; }
        public Nullable<int> POPULATION1990 { get; set; }
        public Nullable<int> POPULATION1980 { get; set; }
        public string COUNTRY { get; set; }
        public Nullable<double> LATITUDE { get; set; }
        public Nullable<double> LONGITUDE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Customer> Customers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Supplier> Suppliers { get; set; }
    }
}
