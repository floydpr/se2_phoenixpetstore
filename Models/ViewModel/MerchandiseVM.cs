﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix_Petstore.Models.ViewModel {
    public class MerchandiseVM {
        public short ITEMID { get; set; }
        public string DESCRIPTION { get; set; }
        public int QUANTITYONHAND { get; set; }
        public string CATEGORY { get; set; }
    }
}