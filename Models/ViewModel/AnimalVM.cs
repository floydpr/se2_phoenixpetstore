﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix_Petstore.Models.ViewModel
{
    public class AnimalVM
    {

        public string NAME { get; set; }
        public string CATEGORY { get; set; }
        public string BREED { get; set; }
        public Nullable<System.DateTime> DATEBORN { get; set; }
        public string GENDER { get; set; }
        public string REGISTERED { get; set; }
        public string COLOR { get; set; }
        public Nullable<double> LISTPRICE { get; set; }
        public string PHOTO { get; set; }
        public string IMAGEFILE { get; set; }
        public Nullable<short> IMAGEHEIGHT { get; set; }
        public string IMAGEWIDTH { get; set; }
    }
}