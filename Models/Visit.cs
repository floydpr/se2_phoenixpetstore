﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix_Petstore.Models
{
    public class Visit
    {
 
        public Visit(string ip, DateTime timestamp, string device)
        {
            Ip = ip;
            Timestamp = timestamp;
            DeviceSource = device;
        }

        public string Ip { get; set; }
        public DateTime Timestamp { get; set; }
        public string DeviceSource { get; set; }

    }
}